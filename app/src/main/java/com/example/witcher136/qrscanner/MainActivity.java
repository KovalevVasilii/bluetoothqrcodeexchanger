package com.example.witcher136.qrscanner;

import android.bluetooth.*;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final UUID uuid = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
    private static final int REQUEST_ENABLE_BT = 1;
    public BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice mDevice;
    private BluetoothDevice curDevice;
    public ConnectedThread mConnectedThread;
    ArrayList<String> listOfDevices;
    EditText sendData;
    TextView viewData;
    Spinner mSpinner;

    public void pairDevice(View v) {

            AcceptThread accept = new AcceptThread();

            accept.start();
            if(curDevice != null){
                Toast.makeText(getApplicationContext(), "Trying to connect to: " + curDevice.getName(), Toast.LENGTH_LONG).show();

                ConnectThread connect = new ConnectThread(curDevice);
                connect.start();


            } else {
                Toast.makeText(getApplicationContext(), "There are no choose devices", Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), "You should create a pair in the settings", Toast.LENGTH_LONG).show();
            }


    }

    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {

            mDevice = device;
        }

        public void run(){
            BluetoothSocket tmp = null;

            try {
                tmp = mDevice.createRfcommSocketToServiceRecord(uuid);
            }
            catch (IOException e) {
                Log.e("MainActivity", "ConnectThread: Could not create socket " + e.getMessage());
            }

            mmSocket = tmp;

            try {
                mmSocket.connect();

            }
            catch (IOException e) {
                try {
                    mmSocket.close();
                }
                catch (IOException e1) {
                    Log.e("MainActivity", "mConnectThread: run: Unable to close connection in socket " + e1.getMessage());
                }
            }
            connected(mmSocket);
        }
        public void cancel() {
            try {
                mmSocket.close();
            }
            catch (IOException e) {
                Log.e("MainActivity", "cancel: close() of mmSocket in Connectthread failed. " + e.getMessage());
            }
        }
    }

    private void connected(BluetoothSocket mmSocket) {

        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.start();
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(final BluetoothSocket socket) {

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

        }

        public void run(){
            byte[] buffer = new byte[1024];

            int bytes;
            /*runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Connected to: "+ mmSocket.getRemoteDevice().getName(), Toast.LENGTH_LONG).show();
                }});*/
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    final String incomingMessage = new String(buffer, 0, bytes);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            viewData.setText(incomingMessage);
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setTitle("Get the message");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            builder.setNeutralButton("Open link", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(incomingMessage));
                                        startActivity(browserIntent);
                                    }
                                    catch(Exception e){
                                        Toast.makeText(getApplicationContext(), "Not a valid link", Toast.LENGTH_LONG).show();

                                    }
                                }
                            });
                            AlertDialog alert1 = builder.create();
                            alert1.show();

                        }
                    });


                }
                catch (IOException e) {
                    Log.e("MainActivity", "write: Error reading Input Stream. " + e.getMessage() );
                    break;
                }
            }
        }


        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            }
            catch (IOException e) {
                Toast.makeText(getApplicationContext(), "There are no active connections", Toast.LENGTH_LONG).show();
                Log.e("MainActivity", "write: Error writing to output stream. " + e.getMessage() );
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            }
            catch (IOException e) { }
        }
    }



    public void SendMessage(View v) {
        byte[] bytes = sendData.getText().toString().getBytes(Charset.defaultCharset());
        if(mConnectedThread == null){
            Toast.makeText(getApplicationContext(), "There are no active connections", Toast.LENGTH_LONG).show();
        }
        else {
            mConnectedThread.write(bytes);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        curDevice = null;

        listOfDevices = new ArrayList<>();
        sendData = findViewById(R.id.editText);
        viewData = findViewById(R.id.textView);

        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            Object[] devices = pairedDevices.toArray();
            for (Object device : devices){
                BluetoothDevice d = (BluetoothDevice) device;
                listOfDevices.add(d.getName());
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "Bound device list is empty", Toast.LENGTH_LONG).show();
            //Intent btSettingsIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
            //startActivityForResult(btSettingsIntent, 1);
        }
        mSpinner= (Spinner) findViewById(R.id.cities);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listOfDevices);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                curDevice = (BluetoothDevice) bluetoothAdapter.getBondedDevices().toArray()[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        mSpinner.setOnItemSelectedListener(itemSelectedListener);
    }


    public void StartServer(View view) {

        Toast.makeText(getApplicationContext(), "Waiting for getting request", Toast.LENGTH_LONG).show();
        AcceptThread accept = new AcceptThread();

        accept.start();


    }

    @Override
    protected void onResume() {
        super.onResume();
        listOfDevices = new ArrayList<>();

        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            Object[] devices = pairedDevices.toArray();
            for (Object device : devices){
                BluetoothDevice d = (BluetoothDevice) device;
                listOfDevices.add(d.getName());
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "Bound device list is empty", Toast.LENGTH_LONG).show();
            //Intent btSettingsIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
            //startActivityForResult(btSettingsIntent, 1);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listOfDevices);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                curDevice = (BluetoothDevice) bluetoothAdapter.getBondedDevices().toArray()[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        mSpinner.setOnItemSelectedListener(itemSelectedListener);
    }

    public void AddToList(View view) {

        Intent btSettingsIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivity(btSettingsIntent);
        listOfDevices = new ArrayList<>();

        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            Object[] devices = pairedDevices.toArray();
            for (Object device : devices){
                BluetoothDevice d = (BluetoothDevice) device;
                listOfDevices.add(d.getName());
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "Bound device list is empty", Toast.LENGTH_LONG).show();
            //Intent btSettingsIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
            //startActivityForResult(btSettingsIntent, 1);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listOfDevices);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                curDevice = (BluetoothDevice) bluetoothAdapter.getBondedDevices().toArray()[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        mSpinner.setOnItemSelectedListener(itemSelectedListener);

    }

    private class AcceptThread extends Thread {

        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread(){
            BluetoothServerSocket tmp = null;

            try{
                tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("appname", uuid);

            }
            catch (IOException e){
                Log.e("MainActivity", "AcceptThread: IOException: " + e.getMessage() );
            }
            catch(Exception e){
                Toast.makeText(getApplicationContext(), "Please, switch on Bluetooth", Toast.LENGTH_LONG).show();
            }


            mmServerSocket = tmp;
        }

        public void run(){
            BluetoothSocket socket = null;

            try{
                socket = mmServerSocket.accept();
            }
            catch (IOException e){
                Log.e("MainActivity", "AcceptThread: IOException: " + e.getMessage() );
            }
            catch(Exception e){
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                    //Toast.makeText(getApplicationContext(), "Please, switch on Bluetooth", Toast.LENGTH_LONG).show();
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }});
            }
            if(socket != null){
                connected(socket);
            }
        }

        public void cancel() {
            try {
                mmServerSocket.close();
            }
            catch (IOException e) {
                Log.e("MainActivity", "cancel: Close of AcceptThread ServerSocket failed. " + e.getMessage() );
            }
        }

    }
    public void ScanQr(View v){
        Intent myIntent = new Intent(MainActivity.this, QRScannerActivity.class);
        MainActivity.this.startActivityForResult(myIntent,2);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null){
            sendData.setText(data.getDataString());

        }
    }
}